import { Component, OnInit } from '@angular/core';
import { NgxSmartModalService } from 'ngx-smart-modal';
import { EventEmitterService } from '../services/event-emitter.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { NgForm, FormBuilder, FormGroup, FormArray, Validators } from '@angular/forms';
import { DataServicesService } from '../services/data-services.service';
import { AuthService } from '../auth/auth.service';

@Component({
  selector: 'app-password-reset',
  templateUrl: './password-reset.component.html',
  styleUrls: ['./password-reset.component.scss']
})
export class PasswordResetComponent implements OnInit {
  navBar: any;
  model: any = {};
  loading: boolean;

  constructor(public eventServices: EventEmitterService,
              private http: HttpClient,
              private authservices: AuthService,
              public dataServices: DataServicesService,
              public ngxSmartModalService: NgxSmartModalService,
              private router: Router,
              private fb: FormBuilder,
              private toastr: ToastrService) { }

ngOnInit() {
  this.navBar = this.eventServices.navchange.emit(true);
}

onSubmitResetPassword(f: NgForm) {
 console.log(f.value);
 if ( f.value.password !== f.value.con_password) {
   this.toastr.error('Password not match' , 'Error!');
   return;
 }
 
 this.loading = true;
 const userToken = this.authservices.getToken();
 const payload = {
  password: f.value.password,
  token: userToken
 };

 this.dataServices.resetPassword(payload).subscribe( success => {
  console.log(success); 
  this.toastr.success('Password saved' , 'Saved!');
  this.loading = false;
  f.resetForm();
},
error => {
 this.toastr.error(error.error.message, 'Error!' );
 this.loading = false;
});

}

}

