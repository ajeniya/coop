import { Component, OnInit } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';
import { EventEmitterService } from '../services/event-emitter.service';
import { ToastrService } from 'ngx-toastr';
import { HttpClient } from '@angular/common/http';
import { NgForm } from '@angular/forms';
import { DataServicesService } from '../services/data-services.service';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';


@Component({
  selector: 'app-superadmin-login',
  templateUrl: './superadmin-login.component.html',
  styleUrls: ['./superadmin-login.component.scss']
})
export class SuperadminLoginComponent implements OnInit {
  loginForm: FormGroup;
  navBar: any;
  loading: boolean;
  model: any = {};
  public loggedIn: any;
  loggingDetail = {};
  loggedSuccess: any;

  constructor(public eventServices: EventEmitterService, private http: HttpClient,
              public dataServices: DataServicesService, private formBuilder: FormBuilder,
              private router: Router, private toastr: ToastrService) {

              }

  ngOnInit() {
    this.navBar = this.eventServices.navchange.emit(false);

    this.loginForm = this.formBuilder.group({
      email: ['', Validators.required],
      password: ['', [Validators.required, Validators.minLength(6)]]
  });
  }

  get f() { return this.loginForm.controls; }

  onSubmit() {
    // stop here if form is invalid
    if (this.loginForm.invalid) {
      this.toastr.error('Detail messing', 'Error!' );
      return;
    }

    this.loading = true;
    this.dataServices.superAdminLogin(this.f.email.value, this.f.password.value)
      .subscribe( returnData => {
            this.loggedSuccess = returnData;
            localStorage.setItem('token', this.loggedSuccess.data.secure);
            this.router.navigate(['/dashboard']);
        },
        error => {
          this.toastr.error(error.error.message, 'Error!' );
          this.loading = false;
        });
  }


}