import { Component, OnInit, OnDestroy } from '@angular/core';
import { NgxSmartModalService } from 'ngx-smart-modal';
import { EventEmitterService } from '../services/event-emitter.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { NgForm, FormBuilder, FormGroup, FormArray, Validators } from '@angular/forms';
import { DataServicesService } from '../services/data-services.service';
import { AuthService } from '../auth/auth.service';

@Component({
  selector: 'app-manage-user',
  templateUrl: './manage-user.component.html',
  styleUrls: ['./manage-user.component.scss']
})
export class ManageUserComponent implements OnInit, OnDestroy {
   private getRoleSubscription: Subscription;
   private deleteRoleSubscription: Subscription;
   rolemodel: any = {};
   p = 1;
   usermodel: any = {
     surname: '',
     othername: '',
     email: '',
     phone: '',
     group: '',
   };

  users = [
    {surname: 'Ademola', other_name: 'Oluwatoyin', email: 'olu@gmail.com', phone: '090876434', group: 'Accounting'},
    {surname: 'Taye', other_name: 'Olukola', email: 'oluk@gmail.com', phone: '07098765310', group: 'Admin'},
    {surname: 'Ademola', other_name: 'Oluwatoyin', email: 'olu@gmail.com', phone: '7683483443', group: 'Accounting'},
    {surname: 'Adunni', other_name: 'Olukola', email: 'olu@gmail.com', phone: '08078651209', group: 'Admin'},
    {surname: 'Omotayo', other_name: 'Seun', email: 'sun@gmail.com', phone: '70348433344', group: 'Accounting'},
    {surname: 'Ikenna', other_name: 'Ogonna', email: 'olu@gmail.com', phone: '0807686532', group: 'Sales'},
    {surname: 'Usman', other_name: 'Adamu', email: 'olu@gmail.com', phone: '07098765409', group: 'Accounting'},
    {surname: 'Ademola', other_name: 'Oluwatoyin', email: 'olu@gmail.com', phone: '09087658791', group: 'Accounting'},
    {surname: 'Professor', other_name: 'Akowe', email: 'olu@gmail.com', phone: '08099997655', group: 'Sales'},
    {surname: 'Remi', other_name: 'Akinolu', email: 'akinolu@gmail.com', phone: '09998373232', group: 'Accounting'},
  ];

  navBar: any;
  loading: boolean;
  model: any = {};
  editData: any;
  showUser = true;
  showgroup = false;
  roleName: string;
  saveRole: any;
  roleList: any;
  userFilter: any = { roles: '' };

  form: FormGroup;
  items: FormArray;


  constructor(public eventServices: EventEmitterService,
              private http: HttpClient,
              private authservices: AuthService,
              public dataServices: DataServicesService,
              public ngxSmartModalService: NgxSmartModalService,
              private router: Router,
              private fb: FormBuilder,
              private toastr: ToastrService) { }

  ngOnInit() {
    this.navBar = this.eventServices.navchange.emit(true);
    this.getRoles();

    this.form = this.fb.group({
      items: this.fb.array([this.createItem()])
    });
  }

  addNext() {
    (this.form.controls['items'] as FormArray).push(this.createItem())
  }
  

  createItem() {
    return this.fb.group({
      name: ['', Validators.required],
      surname: ['', Validators.required]
    });
  }

  onRemove(i) {
    (this.form.controls['items'] as FormArray).removeAt(i);
  }

  onSave( ) {
    console.log('Saving data');
    const Name = this.form.value.name;
    const Value = this.form.value.surname;

    console.log(this.form.value);

    if (this.form.dirty && this.form.valid) {
      console.log(`Surename: ${this.form.value.surname}`);
    }
  }

  public getRoles() {
    this.getRoleSubscription = this.dataServices.getRole()
    .subscribe( success => {
      console.log(success);
      this.roleList = success;
    });
  }

  clearModal() {
    console.log('modal closed');
  }

  onSubmitRole(f: NgForm) {
    const payload = {
      roles: f.value.name,
      roles_description: f.value.description,
      token: this.authservices.getToken()
    };

    this.loading = true;
    this.dataServices.addRole(payload)
      .subscribe( success => {
           this.toastr.success('Role saved' , 'Saved!');
           this.ngxSmartModalService.getModal('group').close();
           this.getRoles();
           this.loading = false;
           f.resetForm();
        },
        error => {
          this.toastr.error(error.error.message, 'Error!' );
          this.loading = false;
          this.ngxSmartModalService.getModal('group').close();
        });
      }

  onSubmitUser(u: NgForm) {
    console.log(u.value);
    const payload = {
      surname: u.value.surname,
      other_name: u.value.othername,
      email: u.value.email,
      phone_number: u.value.phonenumber,
      role_id: u.value.role,
      token: this.authservices.getToken()
    };

    this.loading = true;
    this.dataServices.addUser(payload)
      .subscribe( success => {
           this.toastr.success('Admin user created' , 'Saved!');
           this.ngxSmartModalService.getModal('user').close();
           this.getRoles();
           this.loading = false;
           u.resetForm();
        },
        error => {
          this.toastr.error(error.error.message, 'Error!' );
          this.loading = false;
          this.ngxSmartModalService.getModal('user').close();
        });
  }

  onEditUser(u: NgForm) {
    console.log(u.value);
    u.resetForm();
    this.toastr.success('Saved!', 'Saved Data!');
    this.ngxSmartModalService.getModal('editUser').close();
    // this.toastr.error('Failed', 'Unable to save Data !');
  }

  onEdit(event, user) {
    this.editData = user;
    console.log(user);
    this.usermodel.surname = user.surname;
    this.usermodel.othername = user.other_name;
    this.usermodel.email = user.email;
    this.usermodel.phone = user.phone;
    this.usermodel.group = user.group;
  }

  onRoleSelect(role) {
    this.roleName = role;
    this.showUser = false;
    this.showgroup = true;
  }

  onCloseGroup() {
    this.showUser = true;
    this.showgroup = false;
  }

  onDeleteRole(role) {
    const result = confirm('Are you sure you want to delete.');
    if (result) {
      this.deleteRoleSubscription = this.dataServices.deleteRole(role)
      .subscribe( success => {
           this.toastr.success('Role deleted' , 'Deleted!');
           this.getRoles();
           this.showgroup = false;
           this.showUser = true;
        },
        error => {
          this.toastr.error(error.error.message, 'Error!' );
          this.loading = false;
        });
    }
    

  }

  ngOnDestroy() {
    if (this.getRoleSubscription) {
      this.getRoleSubscription.unsubscribe();
    }
    if (this.deleteRoleSubscription) {
      this.deleteRoleSubscription.unsubscribe();
    }

  }

}
