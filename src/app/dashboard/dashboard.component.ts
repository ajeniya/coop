import { Component, OnInit } from '@angular/core';
import { EventEmitterService } from '../services/event-emitter.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  navBar: any;

  constructor(public eventServices: EventEmitterService) { }

  ngOnInit() {
    this.navBar = this.eventServices.navchange.emit(true);
  }

}
