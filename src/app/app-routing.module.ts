import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { PasswordResetComponent } from './password-reset/password-reset.component';
import { SuperadminLoginComponent } from './superadmin-login/superadmin-login.component';

import { AuthGuardService as AuthGuard } from './auth/auth-guard.service';
import { PagenotfoundComponent } from './pagenotfound/pagenotfound.component';
import { RoleGuardService as RoleGuard } from './auth/role-guard.service';
import { ManageUserComponent } from './manage-user/manage-user.component';

const routes: Routes = [
  { path: '', component: SuperadminLoginComponent },
  { path: 'superadmin-login', component: SuperadminLoginComponent },
  { path: 'dashboard', component: DashboardComponent, canActivate: [AuthGuard]},
  { path: 'manager-user', component: ManageUserComponent, canActivate: [AuthGuard]},
  { path: 'forgot-password', component: ForgotPasswordComponent },
  { path: 'password-reset', component: PasswordResetComponent },
  { path: 'page-not-found', component: PagenotfoundComponent },
  { path: '**', redirectTo: 'page-not-found' }
];

// {  ManageUserComponent
//   path: 'admin', 
//   component: AdminComponent, 
//   canActivate: [RoleGuard], 
//   data: { 
//     expectedRole: 'admin'
//   } 
// },

// PagenotfoundComponent

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
