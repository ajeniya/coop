import { Injectable, EventEmitter } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class EventEmitterService {
  navchange: EventEmitter<boolean> = new EventEmitter();

  constructor() { }
}
