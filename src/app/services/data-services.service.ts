import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import {map, tap} from 'rxjs/operators';

import { ENDPOINTS } from '../app-constants';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class DataServicesService {

  constructor( private http: HttpClient ) { }

 

  superAdminLogin(mail, pass) {
    return this.http.post(`${ENDPOINTS[1]}Superadmin/superadmin_login`, {
      email: mail,
      password: pass
    });
  }


  addRole(payload) {
    return this.http.post(`${ENDPOINTS[1]}Coop_admin/create_roles`, {
      token: payload.token,
      roles: payload.roles,
      roles_description: payload.roles_description
     });
  }


  getRole(): Observable<any> {
    return this.http.get(`${ENDPOINTS[1]}Coop_admin/getroles`).pipe(tap(res => JSON.stringify(res)));
  }

  deleteRole(roleId): Observable<any> {
    return this.http.delete(`${ENDPOINTS[1]}Coop_admin/delete_role/${roleId}`).pipe(tap(res => JSON.stringify(res)));
  }

  resetPassword(payload) {
    return this.http.post(`${ENDPOINTS[1]}Superadmin/resetPassword`, {
      token: payload.token,
      password: payload.password
     });
  } 

  addUser(payload) {
    return this.http.post(`${ENDPOINTS[1]}Superadmin/create_admin`,  payload );
  }

}
