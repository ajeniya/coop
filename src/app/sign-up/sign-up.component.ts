import { Component, OnInit } from '@angular/core';
import { EventEmitterService } from '../services/event-emitter.service';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.scss']
})
export class SignUpComponent implements OnInit {
  navBar: any;

  constructor(public eventServices: EventEmitterService) { }

  ngOnInit() {
    this.navBar = this.eventServices.navchange.emit(false);
  }

}
