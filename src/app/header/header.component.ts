import { Component, OnInit } from '@angular/core';
import { EventEmitterService } from '../services/event-emitter.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  navbarStatus: boolean;
  navbarOpen: any;

  constructor(public eventServices: EventEmitterService,
              private route: ActivatedRoute,
              private router: Router,
          ) { }

  ngOnInit() {
    this.eventServices.navchange.subscribe((data) => {
      this.navbarStatus = data;
    });
  }

  logout() {
    // remove user from local storage to log user out
    localStorage.removeItem('token');
    this.router.navigate(['/superadmin-login']);
}

}
